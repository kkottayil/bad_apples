from torchvision import datasets
import os
import data.utils as utils
import data.processing as processing
import logging
import torch
from glob import glob

class Data_reader():
    '''
    Data reader class for pytorch. Reads a set of images arranged in following directory structure
    train
    ----class 1
    -------img1
    -------img2
    ----class 2
    -------img1
    -------img2

    valid
    ----class 1
    -------img1
    -------img2
    ----class 2
    -------img1
    -------img2
    '''
    def __init__(self, data_dir = 'data\\raw\\', num_workers = 0,  ):
        utils.check_path(data_dir)            
        self.data_dir = data_dir
        self.num_workers = num_workers

    def _read_image_files(self):
        '''
        Assumes that the data is already split into train and val. TODO: Add test
        '''
        composed_transform = processing.get_image_transform()
        image_file_info = {x: datasets.ImageFolder(os.path.join(self.data_dir, x), composed_transform)
                for x in ['train', 'val']}
        self.data_sizes = {x: len(image_file_info[x]) for x in ['train', 'val']}
        self.data_classes = image_file_info['train'].classes
        self.image_file = image_file_info
    
    def print_info(self):
        print(self.image_file)

    def _make_data_loader(self, batch_size ):
        if self.num_workers:
            data_loader = {x: torch.utils.data.DataLoader(self.image_file[x], batch_size=batch_size,
                                                        shuffle=True, num_workers=1)
                            for x in ['train', 'val']}
        else:
            data_loader = {x: torch.utils.data.DataLoader(self.image_file[x], batch_size=batch_size,
                                                        shuffle=True)
                            for x in ['train', 'val']}
        self.data_loader = data_loader

    def get_data_loader(self, batch_size):
        self._read_image_files()
        self._make_data_loader(batch_size)
        return self.data_loader

    def get_classes(self):
        return self.data_classes
    
if __name__ == "__main__":
    #TODO: Move to test
    test = Data_reader("raw\\")
    data_loader = test.get_data_loader()
    test.print_info()

