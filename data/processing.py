from torchvision import models,transforms,datasets

def get_image_transform():
    '''
    Basic transformation of images for compatibility with VGG training.
    '''
    normalize = transforms.Normalize(mean=[0.485, 0.456, 0.406],
                                    std=[0.229, 0.224, 0.225])    
    composed_transform = transforms.Compose([
                transforms.RandomResizedCrop(224),
                transforms.RandomHorizontalFlip(),
                transforms.ToTensor(),
                normalize,
            ])
    return composed_transform