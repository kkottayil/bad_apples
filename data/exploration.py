import utils

class Data_explorer():
    '''
    TODO: Add clusterning and tnsne.
    '''
    def __init__(self, data_dir = 'data\\raw\\' ):
        utils.check_path(data_dir)            
        self.data_dir = data_dir

    def _get_class_names(self):
        class_names = utils.get_subfolder_names(self.data_dir+"train\\")
        return class_names

    def _get_class_counts(self):
        return
    
    def _get_all_image_per_class(self):
        return
    
    def cluster_analysis(self):
        return

    def make_tsne_plot(self):
        return
    
if __name__ == "__main__":
    #TODO: Move to test
    test2 = Data_explorer("raw\\")
    test2._get_class_counts()