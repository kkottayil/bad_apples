import os
import logging
import glob
import cv2
import numpy as np
import torch
from torch.autograd import Variable

def check_path(path):
    try:
        os.path.isfile(path)
    except:
        logging.info("Error: data directory does not exist.")

def get_image_files_in_folder(fpath, img_type = ".jpg"):
    check_path(fpath)
    files = glob.glob(fpath + "*" + img_type)
    return (files)

def read_image(file, size = (100,100) ):
    image = cv2.imread(file)
    if file is not None:
        if size is not None:
            image = cv2.resize(image, size, interpolation = cv2.INTER_AREA)
        return image
    else:
        return None

def read_image_files_in_list(file_list):
    img_list = []
    for file in file_list:
        img = read_image(file)
        if img is not None:
            img_list.append(img)
    all_images = np.stack(img_list)
    return all_images

def get_subfolder_names(folder):
    check_path(folder)
    subfolder_names = [f.name for f in os.scandir(folder) if f.is_dir() ]  
    return subfolder_names

def preprocess_ocv_image(img, grad=False):
    means=[0.485, 0.456, 0.406]
    stds=[0.229, 0.224, 0.225]

    preprocessed_img = img.copy()[: , :, ::-1]
    for i in range(3):
        preprocessed_img[:, :, i] = preprocessed_img[:, :, i] - means[i]
        preprocessed_img[:, :, i] = preprocessed_img[:, :, i] / stds[i]
    preprocessed_img = \
        np.ascontiguousarray(np.transpose(preprocessed_img, (2, 0, 1)))
    preprocessed_img = torch.from_numpy(preprocessed_img)
    preprocessed_img.unsqueeze_(0)
    input = Variable(preprocessed_img, requires_grad = grad)
    return input

# files = get_image_files_in_folder("D:\\Documents\\python\\ML workflow\\data\\raw\\val\\rotten apple\\")
# img = read_image(files[0])
# read_image_files_in_list(files)
# print(get_subfolder_names("D:\\Documents\\python\\ML workflow\\data\\raw\\val"))