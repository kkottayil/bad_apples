import torch
import torch.nn as nn
from torch.autograd import Variable
import torchvision
from torchvision import models,transforms,datasets
import numpy as np
import matplotlib.pyplot as plt
import os
import torch.optim as optim
from torch.optim import lr_scheduler
from data import utils
import cv2


class transfer_learning_model():
    '''
    Simple transfer learning model that is obtained by modifying the FC layer of VGG16.
    '''
    def __init__(self, out_features = 2, lr=0.0001, momentum=0.9, lr_decay_step = 7, lr_decay_gamma = 0.1):
        self.out_features = out_features
        self.lr = lr
        self.momentum = momentum
        self.lr_decay_step = lr_decay_step
        self.lr_decay_gamma = lr_decay_gamma
        
    def _make_base_model(self):
        self.model = models.vgg16(pretrained=True)

    def _modify_model_fc(self):
        self.model.classifier[-1] = nn.Linear(in_features=4096, out_features=self.out_features)
        
    def make_model(self):
        '''
        Load VGG and modify the FC layer to have specicfied number of outputs.
        '''
        self._make_base_model()
        self._modify_model_fc()
        self.set_parameters(self.lr, self.momentum, self.lr_decay_step, self.lr_decay_gamma)
        
    def print_model(self):
        print(self.model)
        print("Learning rate, momentum, decay step, decay gamma: ", self.lr, self.momentum, self.lr_decay_step, self.lr_decay_gamma)
        print("Optimizer type: ", self.optimizer_ft )
        print("Loss: ", self.criterion)
        
    def get_model(self):
        return self.model
    
    def set_parameters(self, lr, momentum, lr_decay_step, lr_decay_gamma):
        '''
        Set the hyperparameters of the ML model.
        '''        
        #TODO: add methods for independently defining optimizers and lr
        self.optimizer_ft = optim.SGD(self.model.parameters(), lr=lr, momentum=momentum)
        self.exp_lr_scheduler = lr_scheduler.StepLR(self.optimizer_ft, step_size=7, gamma=0.1)
        # Decay LR by a factor of 0.1 every 7 epochs
        self.exp_lr_scheduler = lr_scheduler.StepLR(self.optimizer_ft, step_size=lr_decay_step, gamma=lr_decay_gamma)
        self.criterion = nn.CrossEntropyLoss()

    def save_model(self, save_path = "models\\weights", name = "\\model.pth"):
        try:
            os.stat(save_path)
        except:
            os.mkdir(save_path)   
        torch.save(self.model.state_dict(), save_path + name)

    def load_model(self,  save_path = "models\\weights", name = "\\model.pth"):
        self.model.load_state_dict(torch.load(save_path + name))
        self.model.eval()

    def predict_on_image(self, fname, device = 'cpu' ):     
        '''
        Predict the class of a single image.
        '''
        img = cv2.imread(fname, 1)
        # TODO: Add checks
        img = np.float32(cv2.resize(img, (224, 224))) / 255
        input = utils.preprocess_ocv_image(img)
        # TODO: Add intelligent device check
        self.model = self.model.to(device)
        outputs = self.model(input.to(device))
        _, preds = torch.max(outputs, 1)
        output_class = preds.cpu().numpy()
        #print(preds)
        return output_class
        
