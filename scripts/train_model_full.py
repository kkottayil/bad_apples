import data.reader as datareader
import torchvision
import numpy as np
import matplotlib.pyplot as plt
import models.model as model
import models.training as trainer
import torch
import logging
import pandas as pd
import argparse
'''
Script for training TL model for classification of good vs bad apples.
'''

def main(args):
    logging.basicConfig(filename="D:\\Documents\\python\\bad_apples\\logs\\logs_train.log", level=logging.INFO)
    logging.info("Start")
    data_handler = datareader.Data_reader("D:\\Documents\\python\\bad_apples\\data\\raw\\")
    tl_model = model.transfer_learning_model(lr=args.lr, momentum=args.momentum, lr_decay_step = args.decay_step, lr_decay_gamma = args.decay_gamma)
    tl_model.make_model()
    acc = trainer.train_model(model = tl_model.model, criterion = tl_model.criterion, optimizer = tl_model.optimizer_ft, 
                            scheduler = tl_model.exp_lr_scheduler, data_handler = data_handler, 
                            batch_size = args.batch_size,  num_epochs=args.epochs)
    tl_model.save_model(name = args.model_save_name)
    perf_log_location = args.perf_log_location

    my_dict = {'validation error': acc}
    with open(perf_log_location, 'w') as f:
        for key in my_dict.keys():
            f.write("%s: %s\n"%(key,my_dict[key]))


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Bad Apples')
    parser.add_argument('--batch_size', type=int, default=8, metavar='N',
                        help='input batch size for training (default: 64)')
    parser.add_argument('--epochs', type=int, default=10, metavar='N',
                        help='number of epochs to train (default: 10)')
    parser.add_argument('--lr', type=float, default=0.0001, metavar='LR',
                        help='learning rate (default: 0.0001)')
    parser.add_argument('--momentum', type=float, default=0.9, metavar='M',
                        help='SGD momentum (default: 0.9)')
    parser.add_argument('--decay_step',type=float, default=7, metavar='M',
                        help='Steps to next decay for learning rate (default: 7)')
    parser.add_argument('--decay_gamma', type=float, default=0.1, metavar='M',
                        help='Scaling of learning rate after step (default: 0.1)')
    
    parser.add_argument('--model_save_folder', type=str, default="weights", metavar='S',
                        help='Folder to save model')
    parser.add_argument('--model_save_name', type=str, default="\\model_train_full.pth", metavar='S',
                        help='Folder to save model')
    parser.add_argument('--perf_log_location', type=str, default="D:\\Documents\\python\\bad_apples\\logs\\train.csv", metavar='S',
                        help='Path to save model performance')
    args = parser.parse_args()

    main(args)

