import data.reader as datareader
import torchvision
import numpy as np
import matplotlib.pyplot as plt
import models.model as model
import models.training as trainer
import torch
import logging
import pandas as pd
'''
Script for a grid search of hyperparameteters. Performance of each config is logged in a .csv file. 
TODO: Convert to proper script
'''
# Setup logger
logging.basicConfig(filename="D:\Documents\\python\\bad_apples\\logs\\logs_tune.log", level=logging.DEBUG)
logging.info("Start")

# Setup data reader
data_handler = datareader.Data_reader("D:\Documents\\python\\bad_apples\\data\\raw\\")

# Set up search space
lr_space = [0.001, 0.0001]
momentum_space = [0.8,0.9]
decay_space = [6,7]
gamma_space = [0.01,0.1]
epoch_space = [2,4,6,8,10]

# Simple grid search
all_performance = []
for lr in lr_space:
    for momentum in momentum_space:
        for decay in decay_space:
            for gamma in gamma_space:  
                for nepoch in epoch_space:              
                    tl_model = model.transfer_learning_model(lr=lr, 
                                                            momentum=momentum, 
                                                            lr_decay_step = decay, 
                                                            lr_decay_gamma = gamma)
                    tl_model.make_model()
                    acc = trainer.train_model(tl_model.model, tl_model.criterion, 
                                                            tl_model.optimizer_ft, 
                                                            tl_model.exp_lr_scheduler, 
                                                            data_handler,
                                                            num_epochs=nepoch)
                    
                    all_performance.append(list([nepoch, lr, momentum, decay, gamma, acc]))
                    # save results
                    perf_df = pd.DataFrame(np.stack(all_performance), columns = ['nepoch','LR','mom','dec','gamma','acc'])
                    perf_df.to_csv("D:\Documents\\python\\bad_apples\\logs\\tuning.csv")
