# Bad apples
Pet project that uses a modified VGG16 neural net to train a classifier that detects rotten apples.
The rotten regions are then hilighted using gradCAM. 
Example shown in: notebooks/gradcam.ipynb

Scripts for 
1. Hyperparameter tuning: scripts/tune_mode.py
2. Training: train_model_full.py

